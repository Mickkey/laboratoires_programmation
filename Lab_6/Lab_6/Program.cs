﻿//Algorithme d’une simulation d’un jeu impliquant 2 joueurs, où à chaque tour les joueurs choisissent un nombre entre 1 et 25, le joueur ayant le plus grand nombre remportant la manche.
//Les joueurs commencent avec 60$ et doivent miser 5$ à chaque manche.
//Le jeu se déroule jusqu’à ce qu’un joueur n’ait plus d’argent, ou qu’il a gagné 10 manches.

//Par Mickaël Sautron
//Le 26 octobre 16


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variables représentant l'argent des joueurs
            int argent_j1;
            int argent_j2;
            //Variables qui vont stocker les nombres des joueurs à chaque tour
            int nombre_j1;
            int nombre_j2;

            int manche; //Variable qui va stocker le numéro de la manche
            int manches_j1; //Variable qui va stocker le nombre de manches gagnées par le joueur 1
            int manches_j2;
            Random rand = new Random(); //Objet Random nécessaire à la génération des nombres aléatoires

            //On initialise l'argent des joueurs, leurs nombres de victoires, et le numero de la manche actuelle
            argent_j1 = 60;
            argent_j2 = 60;
            manche = 1;
            manches_j1 = 0;
            manches_j2 = 0;

            //Les joueurs ne peuvent pas perdre par manque d'argent, car ils doivent dépenser maximum 50$ (10 défaites de 5$)
            //La condition d'arrêt pour la boucle doit donc tester si aucun joueur n'a gagné 10 fois
            while (manches_j1 < 10 && manches_j2 < 10)
            {
                //Les joueurs tirent leurs nombres
                nombre_j1 = rand.Next(1, 26);
                nombre_j2 = rand.Next(1, 26);

                //On leur soustrait l'argent de leur mise
                argent_j1 -= 5;
                argent_j2 -= 5;

                Console.WriteLine("Début de la manche {0}", manche);
                Console.WriteLine("Le joueur 1 a choisi le nombre {0}", nombre_j1);
                Console.WriteLine("Le joueur 2 a choisi le nombre {0}", nombre_j2);

                if (nombre_j1 < nombre_j2) //Si le joueur 2 gagne la manche
                {
                    argent_j2 += 10; //Il gagnent l'argent misé par les 2 joueurs
                    manches_j2++; //On incrémente donc son nombre de victoires
                    Console.WriteLine("Le joueur 2 remporte la manche {0} ! ({1} win)\n", manche, manches_j2);
                }
                else if (nombre_j1 > nombre_j2) //Même chose, si le joueur 1 gagne
                {
                    argent_j1 += 10;
                    manches_j1++;
                    Console.WriteLine("Le joueur 1 remporte la manche {0}! ({1} win)\n", manche, manches_j1);
                }
                else //S'il y a égalité
                {
                    //Les deux joueurs reprennent leur mises
                    argent_j1 += 5;
                    argent_j2 += 5;
                    Console.WriteLine("Les deux joueurs ont choisi le même nombre, les joueurs reprennent leur mise\n");
                }
                manche++; //On incrémente le compteur de manches
            }

            //La boucle est terminée, il y a donc deux possibilités:

            if (manches_j2 == 10) //Le joueur 2 est arrivé à 10 manches
            {
                Console.WriteLine("Le joueur 2 a gagné, il a atteint 10 manches gagnées\n");
            }
            else //Ou alors c'est joueur 1 qui a gagné 10 manches
            {
                Console.WriteLine("Le joueur 1 a gagné, il a atteint 10 manches gagnées\n");
            }

            Console.ReadKey(); //On attend que l'utilisateur appuie sur une touche afin que le programme ne se termine pas brusquement
        }
    }
}
